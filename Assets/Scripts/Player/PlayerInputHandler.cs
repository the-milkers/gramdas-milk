using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

namespace Player
{
    public class PlayerInputHandler : MonoBehaviour
    {
        private PlayerInput _playerInput;
        private PlayerMovement _mover;

        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
            var movers = FindObjectsOfType<PlayerMovement>();
            var index = _playerInput.playerIndex;
            _mover = movers.FirstOrDefault(m => m.GetPlayerIndex() == index);
        }

        public void OnMove(CallbackContext context)
        {
            if(_mover != null)
                _mover.SetInputVector(context.ReadValue<Vector2>());
        }
    }
}
