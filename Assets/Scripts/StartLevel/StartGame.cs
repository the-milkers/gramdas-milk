using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGame : MonoBehaviour
{
    [SerializeField] private Canvas startCanvas;
    [SerializeField] private Canvas levelSelectCanvas;
    
    public void OnClick()
    {
        startCanvas.gameObject.SetActive(false);
        levelSelectCanvas.gameObject.SetActive(true);
    }
}
