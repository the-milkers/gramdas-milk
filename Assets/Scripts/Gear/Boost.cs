using Player;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Boost : MonoBehaviour
{
    public enum boostType { speedboost, jumpboost }
    public enum Player { player1, player2 }

    private const string PrimaryGearKey = "_PrimaryGear";
    private const string SecondaryGearKey = "_SecondaryGear";

    [Header("Settings")] 
    public float boostMultiplier = 1.1f;
    public boostType boost;

    [Header("Cooldown")]
    public float activationcooldown = 20f;
    public float activationCDTimer = 20f;

    [Header("Timer")]
    public float activationDuration = 5f;
    public float activationTimer = 5f;


    bool _boostActivated = false;
    float _standardSpeed;
    float _standardJumpForce;
    bool _player1Activated;

    PlayerMovement _player1;
    PlayerMovement _player2;

    private KeyCode _Player1Activation;
    private KeyCode _Player2Activation;

    private int _index;
    private Gear _gear;
    private GearSettings _gearSettings;

    bool _primary;
    private void Start()
    {
        _gear = GetComponent<Gear>();
        _gearSettings = FindObjectOfType<GearSettings>();

        foreach (var player in FindObjectsOfType<PlayerMovement>())
        {
            if (player.GetPlayerName() == "Player") _player1 = player;
            if (player.GetPlayerName() == "Player2") _player2 = player;
        }
        _standardSpeed = FindObjectOfType<PlayerMovement>().GetSpeed();
        _standardJumpForce = FindObjectOfType<PlayerMovement>().GetJumpForce();

        _index = _gear.GetIndex();
        SetKeyCodes();
    }

    private void Update()
    {
        //checking if players have started moving
        if (FindObjectOfType<PlayerInputHandler>() == null) return;

        //checks if the input gets activated
        if (Input.GetKeyDown(_Player1Activation)) ActivateBoost(Player.player1);
        if (Input.GetKeyDown(_Player2Activation)) ActivateBoost(Player.player2);

        //starts the cool down timer
        if (activationCDTimer > 0) activationCDTimer -= Time.deltaTime;

        //starts the activation timer
        if (_boostActivated && activationTimer > 0) activationTimer -= Time.deltaTime;

        //stops the boost if the timer hits 0
        if(activationTimer < 0) stopBoost();
    }

    /// <summary>
    /// Activates the boost of the player, returns nothing when cooldown timer hasnt hit 0 yet
    /// </summary>
    /// <param name="player"></param>
    private void ActivateBoost(Player player)
    {
        if (activationCDTimer > 0) return;
        if(!_boostActivated) StartBoost(player);
        FindObjectOfType<GearUI>().OnActivation(_primary);
    }

    /// <summary>
    /// starts the boost depending on which player activated it, and then gives it to that player
    /// </summary>
    /// <param name="player"></param>
    private void StartBoost(Player player)
    {
        _boostActivated = true;
        if(player == Player.player1)
        {
            _player1Activated = true;
            if (boost == boostType.speedboost) _player1.SetSpeed(_standardSpeed * boostMultiplier);
            if (boost == boostType.jumpboost) _player1.SetJumpForce(_standardJumpForce * boostMultiplier);
        }
        if(player == Player.player2)
        {
            _player1Activated = false;
            if (boost == boostType.speedboost) _player2.SetSpeed(_standardSpeed * boostMultiplier);
            if (boost == boostType.jumpboost) _player2.SetJumpForce(_standardJumpForce * boostMultiplier);
        }
    }

    /// <summary>
    /// sets the speed or jumpforce of the player back to its original
    /// </summary>
    private void stopBoost()
    {
        FindObjectOfType<GearUI>().OnCoolDownStart(_primary, activationcooldown);
        _boostActivated = false;
        activationCDTimer = activationcooldown;
        activationTimer = activationDuration;

        if (_player1Activated)
        {
            if (boost == boostType.speedboost) _player1.SetSpeed(_standardSpeed);
            if (boost == boostType.jumpboost) _player1.SetJumpForce(_standardJumpForce);
        }
        else if (!_player1Activated)
        {
            if (boost == boostType.speedboost) _player2.SetSpeed(_standardSpeed);
            if (boost == boostType.jumpboost) _player2.SetJumpForce(_standardJumpForce);
        }
    }

    public void onDeath()
    {
        _boostActivated = false;
        activationCDTimer = 0;
        activationTimer = activationDuration;

        if (_player1Activated)
        {
            if (boost == boostType.speedboost) _player1.SetSpeed(_standardSpeed);
            if (boost == boostType.jumpboost) _player1.SetJumpForce(_standardJumpForce);
        }
        else if (!_player1Activated)
        {
            if (boost == boostType.speedboost) _player2.SetSpeed(_standardSpeed);
            if (boost == boostType.jumpboost) _player2.SetJumpForce(_standardJumpForce);
        }
    }

    public void SetKeyCodes(KeyCode p1, KeyCode p2)
    {
        _Player1Activation = p1;
        _Player2Activation = p2;
    }

    private void SetKeyCodes()
    {
        if (_index == PlayerPrefs.GetInt(PrimaryGearKey))
        {
            _Player1Activation = _gearSettings.getP1PrimaryKeyCode();
            _Player2Activation = _gearSettings.getP2PrimaryKeyCode();
            _primary = true;
        }
        else if (_index == PlayerPrefs.GetInt(SecondaryGearKey))
        {
            _Player1Activation = _gearSettings.getP1SecondaryKeyCode();
            _Player2Activation = _gearSettings.getP2SecondaryKeyCode();
            _primary = false;
        }
    }

}
