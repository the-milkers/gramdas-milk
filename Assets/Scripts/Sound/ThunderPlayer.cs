using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ThunderPlayer : MonoBehaviour
{
    [Header("AudioClips")]
    public AudioClip[] rolls;
    public AudioClip[] Strikes;

    [Header("AudioSource")]
    public AudioSource RollSource;
    public AudioSource StrikeSource;

    [Header("Light")]
    public Light StrikeLight;

    private int currentstrike;

    private float TimerRoll;
    private float TimerStrike;

    private float TimeRoll;
    private float TimeStrike;

    private float LightTimer;
    private bool PlayStrikeLight;
    private bool InNature;
    private float[] LightIntensity = {0.8f, 0.6f, 0.4f};


    private void Start()
    {
        TimeRoll = Random.Range(5f, 10f);
        TimeStrike = Random.Range(10f, 15f);
    }

    void Update()
    {
        TimerRoll -= Time.deltaTime;
        TimerStrike -= Time.deltaTime;

        if(TimerRoll < 0)
        {
            RollSource.clip = rolls[Random.Range(0, rolls.Length)];
            RollSource.Play();
            TimeRoll = Random.Range(2.5f, 5f) + RollSource.clip.length;
            TimerRoll = TimeRoll;
        }
        if (TimerStrike < 0)
        {
            currentstrike = Random.Range(0, Strikes.Length);
            StrikeSource.clip = Strikes[currentstrike];
            PlayStrikeLight = true;
            StrikeSource.Play();
            TimeStrike = Random.Range(2.5f, 5f) + StrikeSource.clip.length;
            TimerStrike = TimeStrike;
        }
        if (PlayStrikeLight && !InNature)
        {
            
            LightTimer += Time.deltaTime;
            StrikeLight.gameObject.SetActive(true);
            if(LightTimer > .6f)
            {
                StrikeLight.intensity = LightIntensity[currentstrike];
                StrikeLight.gameObject.SetActive(false);
                LightTimer = 0;
                PlayStrikeLight = false;
            }
        }

    }

    public void OnNatureEnter()
    {
        InNature = true;
    }
    public void OnNatureExit()
    {
        InNature = false;
    }
   
}
