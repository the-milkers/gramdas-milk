using System;
using UnityEngine;

namespace Player
{
    public class PlayerInventory : MonoBehaviour
    {
        private const String CoinKey = "Coins";

        private int CurrentCoins { get; set; }

        private void Awake()
        {
            CurrentCoins = PlayerPrefs.GetInt(CoinKey);
        }

        public void AddCoins(int coins)
        {
            PlayerPrefs.SetInt(CoinKey, CurrentCoins + coins);
        }

        public int GetCoins()
        {
            return CurrentCoins;
        }
    }
}
