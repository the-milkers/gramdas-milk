using UnityEngine;
using UnityEngine.SceneManagement;

public class levelselect : MonoBehaviour
{
    [SerializeField] Canvas LevelSelectCanvas;
    [SerializeField] Canvas StartScreenCanvas;

    private void Start()
    {
        LevelSelectCanvas.gameObject.SetActive(false);
        StartScreenCanvas.gameObject.SetActive(true);
    }

    public void ShowLevelSelect()
    {
        LevelSelectCanvas.gameObject.SetActive(true);
        StartScreenCanvas.gameObject.SetActive(false);
    }

    public void StartTutorial()
    {
        ///TODO: goeie scene laden
        SceneManager.LoadScene(0);
    }






}
