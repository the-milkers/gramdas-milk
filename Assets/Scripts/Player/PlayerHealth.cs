using System;
using Player;
using UnityEngine;
using UnityEngine.UI;
using Util;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] private GameObject healthUi;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Canvas gameOverCanvas;

    private int _lives = 3;
    private PauseGame _pauseGame;
    private SFXManager _sfx;

    private void Awake()
    {
        _pauseGame = FindObjectOfType<PauseGame>();
        _sfx = FindObjectOfType<SFXManager>();
    }

    public void OnTakeDamage()
    {
        _lives--;
        if (_lives <= 0)
        {
            _sfx.PlayGameOverAudio();
            gameOverCanvas.gameObject.SetActive(true);
            _pauseGame.SetHasMenuOpen(true);
            mainCamera.GetComponent<Blur>().enabled = true;
            FindObjectOfType<Timer>().EndTimer();
            FindObjectOfType<PointSystem>().onFullDeath();
            foreach (var playerMovement in transform.parent.GetComponentsInChildren<PlayerMovement>())
            {
                playerMovement.enabled = false;
            }
        }
        switch (_lives)
        {
            case 2:
                Transform healthImage2 = healthUi.transform.Find("Heart2");
                healthImage2.GetComponent<Image>().color = Color.gray;
                break;
            case 1:
                Transform healthImage1 = healthUi.transform.Find("Heart1");
                healthImage1.GetComponent<Image>().color = Color.gray;
                break;
            case 0:
                Transform healthImage = healthUi.transform.Find("Heart");
                healthImage.GetComponent<Image>().color = Color.gray;
                break;
        }
    }

    public int GetPlayerDeaths()
    {
        return (3 - _lives);
    }
}
