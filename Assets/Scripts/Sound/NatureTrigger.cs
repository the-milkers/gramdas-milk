using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class NatureTrigger : MonoBehaviour
{
    [Header("Audio")]
    public AudioMixerSnapshot NatureIN;
    public AudioMixerSnapshot NatureOUT;

    [Header("Settings")]
    public float TransistionIn;
    public float TransitionOut;

    ThunderPlayer thunder;
    private void Start()
    {
        thunder = FindObjectOfType<ThunderPlayer>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            NatureIN.TransitionTo(TransitionOut);
            thunder.OnNatureEnter();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            NatureOUT.TransitionTo(TransitionOut);
            thunder.OnNatureExit();
        }
    }
}
