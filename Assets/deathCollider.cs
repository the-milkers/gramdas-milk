using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deathCollider : MonoBehaviour
{
    DeathHandler deathHandler;

    private void Start()
    {
        deathHandler = FindObjectOfType<DeathHandler>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag != "Player" && other.gameObject.tag != "SpawnPoint" && other.gameObject.tag != "Pickup" && other.gameObject.tag != "Interactable")
        {
            //deathHandler.HandleDeath(gameObject.GetComponent<Collider>());
        }
    }
}
