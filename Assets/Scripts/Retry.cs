using Player;
using UnityEngine;
using Util;

public class Retry : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Canvas self;
    [SerializeField] private GameObject players;

    private SceneLoader _sceneLoader;

    private void Start()
    {
        _sceneLoader = FindObjectOfType<SceneLoader>();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0))
        {
            _sceneLoader.LoadScene();
        }
    }
}
