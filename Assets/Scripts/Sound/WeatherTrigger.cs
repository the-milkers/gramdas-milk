using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class WeatherTrigger : MonoBehaviour
{

    [Header("Audio")]
    public AudioMixerSnapshot InsideSnapshot;
    public AudioMixerSnapshot OutsideSnapshot;
    public AudioSource WeatherSource;

    [Header("Settings")]
    public float TransistionIn;
    public float TransitionOut;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            OutsideSnapshot.TransitionTo(TransitionOut);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            InsideSnapshot.TransitionTo(TransitionOut);
        }
    }
}
