using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GearUI : MonoBehaviour
{
    private const string PrimaryGearKey = "_PrimaryGear";
    private const string SecondaryGearKey = "_SecondaryGear";

    [SerializeField] Canvas UserInterfaceCanvas;

    Image _primaryCDTimerIMG;
    Image _secondaryCDTimerIMG;

    float _cdtimerP;
    float _cooldownP;

    float _cdtimerS;
    float _cooldownS;

    bool _startTimerP = false;
    bool _startTimerS = false;

    


    private void Start()
    {
        _primaryCDTimerIMG = GetRingBarImgPrimary();     
        _secondaryCDTimerIMG = GetRingBarImgSecondary();

        _primaryCDTimerIMG.fillAmount = 0;
        _secondaryCDTimerIMG.fillAmount = 0;

        updateGearTXT();

    }
    private void Update()
    {
        if (_startTimerP)
        {
            _cdtimerP -= Time.deltaTime;
            _primaryCDTimerIMG.fillAmount = _cdtimerP / _cooldownP;
            if( _cdtimerP <= 0)
            {
                _primaryCDTimerIMG.fillAmount = 0;
                GetGearPanelIMGPrimary().color = new Color(1, 1, 1, .9f);
                _startTimerP = false;
            }
        }
        if (_startTimerS)
        {
            _cdtimerS -= Time.deltaTime;
            _secondaryCDTimerIMG.fillAmount = _cdtimerS / _cooldownS;
            if (_cdtimerS <= 0)
            {
                _secondaryCDTimerIMG.fillAmount = 0;
                GetGearPanelIMGSecondary().color = new Color(1, 1, 1, .9f);
                _startTimerS = false;
            }
        }
    }

    public void OnCoolDownStart(bool primary, float cooldown)
    {
        if (primary)
        {
            _startTimerP = true;
            _cooldownP = cooldown;
            _cdtimerP = cooldown;
            
        }
        if (!primary)
        {
            _startTimerS = true;
            _cooldownS = cooldown;
            _cdtimerS = cooldown;
            
        }
    }

    public void OnActivation(bool primary)
    {
        if (primary) GetGearPanelIMGPrimary().color = new Color(1, 1, 1, .3f);
        if (!primary) GetGearPanelIMGSecondary().color = new Color(1, 1, 1, .3f);
    }

    public void ResetGear()
    {
        _startTimerP = false;
        _startTimerS = false;
        GetGearPanelIMGPrimary().color = new Color(1, 1, 1, .9f);
        GetGearPanelIMGSecondary().color = new Color(1, 1, 1, .9f);
        _secondaryCDTimerIMG.fillAmount = 0;
        _primaryCDTimerIMG.fillAmount = 0;
    }




    private Image GetRingBarImgPrimary()
    {
        return UserInterfaceCanvas.transform.Find("Gear").transform.Find("Primary").transform.Find("CDtimerImg").gameObject.GetComponent<Image>();
    }
    private Image GetRingBarImgSecondary()
    {
        return UserInterfaceCanvas.transform.Find("Gear").transform.Find("Secondary").transform.Find("CDtimerImg").gameObject.GetComponent<Image>();
    }

    private Image GetGearPanelIMGPrimary()
    {
        return UserInterfaceCanvas.transform.Find("Gear").transform.Find("Primary").gameObject.GetComponent<Image>();
    }

    private Image GetGearPanelIMGSecondary()
    {
        return UserInterfaceCanvas.transform.Find("Gear").transform.Find("Secondary").gameObject.GetComponent<Image>();
    }

    private void SetPrimaryGearTXT(string GearName)
    {
        GetGearPanelIMGPrimary().transform.Find("PrimaryTXT").GetComponent<TextMeshProUGUI>().text = GearName;
    }
    private void SetSecondaryGearTXT(string GearName)
    {
        GetGearPanelIMGSecondary().transform.Find("SecondaryTXT").GetComponent<TextMeshProUGUI>().text = GearName;
    }

    private void updateGearTXT()
    {
        switch (PlayerPrefs.GetInt(PrimaryGearKey))
        {
            case 0:
                SetPrimaryGearTXT("");
                break;
            case 1:
                SetPrimaryGearTXT("Speed Boost");
                break;
            case 2:
                SetPrimaryGearTXT("Jump Boost");
                break;
            case 3:
                SetPrimaryGearTXT("Dash");
                break;
        }
        switch (PlayerPrefs.GetInt(SecondaryGearKey))
        {
            case 0:
                SetSecondaryGearTXT("");
                break;
            case 1:
                SetSecondaryGearTXT("Speed Boost");
                break;
            case 2:
                SetSecondaryGearTXT("Jump Boost");
                break;
            case 3:
                SetSecondaryGearTXT("Dash");
                break;
        }
    }
}
