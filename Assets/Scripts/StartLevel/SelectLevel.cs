using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SelectLevel : MonoBehaviour
{
    [SerializeField] Canvas LevelSelectCanvas;

    GameObject _levelselectpanel;
    GameObject _tutorialbtn;
    GameObject _level1btn;
    GameObject _level2btn;

    int _selectedLevel;

    void Start()
    {
        _levelselectpanel = LevelSelectCanvas.transform.Find("LevelSelectPanel").gameObject;

        _tutorialbtn = _levelselectpanel.transform.Find("TutorialBTN").gameObject;
        _level1btn = _levelselectpanel.transform.Find("Level1BTN").gameObject;
        _level2btn = _levelselectpanel.transform.Find("Level2BTN").gameObject;
    }

    void Update()
    {
        setColor();
    }



    public void onLevelStart()
    {
        if (_tutorialbtn.GetComponent<Toggle>().isOn) _selectedLevel = 1;
        if (_level1btn.GetComponent<Toggle>().isOn) _selectedLevel = 2;
        if (_level2btn.GetComponent<Toggle>().isOn) _selectedLevel = 3;

        SceneManager.LoadScene(_selectedLevel);
    }

    private void setColor()
    {
        if (_tutorialbtn.GetComponent<Toggle>().isOn) _tutorialbtn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _tutorialbtn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);

        if (_level1btn.GetComponent<Toggle>().isOn) _level1btn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _level1btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);

        if (_level2btn.GetComponent<Toggle>().isOn) _level2btn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _level2btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);
    }

    

}
