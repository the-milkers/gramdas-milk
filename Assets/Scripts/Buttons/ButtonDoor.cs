using System;
using System.Collections.Generic;
using Objects;
using UnityEngine;

namespace Buttons
{
    public class ButtonDoor : MonoBehaviour
    {
        [SerializeField] private Door door;

        private static readonly int IsPushed = Animator.StringToHash("IsPushed");
        private readonly List<Collider> _colliders = new();
        private bool IsTriggered => _colliders.Count > 0;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag("Player") && !other.gameObject.CompareTag("Moveable")) return;
            _colliders.Add(other);
            GetComponent<Animator>().SetBool(IsPushed, true);
            door.Activate();
        }

        private void OnTriggerExit(Collider other)
        {
            _colliders.Remove(other);
            if (IsTriggered) return;
            GetComponent<Animator>().SetBool(IsPushed, false);
            door.DeActivate();
        }
    }
}
