using System;
using Player;
using UnityEngine;

namespace Objects
{
    public class CoinPickup : MonoBehaviour
    {
        [SerializeField] private int coinValue = 1;
        private bool _pickedUpInPreviousRun;

        private PlayerInventory _playerInventory;
        private CoinManager _coinManager;

        private void Awake()
        {
            _playerInventory = FindObjectOfType<PlayerInventory>();
            _coinManager = FindObjectOfType<CoinManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("Player")) return;
            if (!_pickedUpInPreviousRun)
            {
                _pickedUpInPreviousRun = true;
                _playerInventory.AddCoins(coinValue);
            }
            _coinManager.OnCoinPickup(coinValue);
            Destroy(gameObject);
        }

        public bool GetPickedUpInPreviousRun()
        {
            return _pickedUpInPreviousRun;
        }

        public void SetPickedUpInPreviousRun(bool pickedUp)
        {
            _pickedUpInPreviousRun = pickedUp;
        }
    }
}
