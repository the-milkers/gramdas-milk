using System;
using UnityEngine;

namespace Objects
{
    public class Door : MonoBehaviour
    {
        private static readonly int IsOpen = Animator.StringToHash("IsOpen");

        public void Activate()
        {
            GetComponent<Animator>().SetBool(IsOpen, true);
        }

        public void DeActivate()
        {
            GetComponent<Animator>().SetBool(IsOpen, false);
        }
    }
}
