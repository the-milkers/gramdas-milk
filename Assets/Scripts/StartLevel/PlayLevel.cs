using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayLevel : MonoBehaviour
{

    SelectGear _selectGear;
    SelectLevel _selectLevel;


    void Start()
    {
        _selectGear = FindObjectOfType<SelectGear>();
        _selectLevel = FindObjectOfType<SelectLevel>();
    }

    public void Play()
    {
        _selectGear.OnLevelStart();
        _selectLevel.onLevelStart();
    }
}
