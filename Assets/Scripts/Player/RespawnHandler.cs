using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnHandler : MonoBehaviour
{
    [SerializeField] GameObject spawnpoint;
    [SerializeField] GameObject AllSpawnPoints;
    [SerializeField] Camera cam;
    int spawnpointsCleared = 1;



    private void Update()
    {
        
    }

    public void ChangeSpawnPoint(GameObject _NewSpawnPoint)
    {
        if (_NewSpawnPoint.GetComponent<SpawnPoint>().GetIndex() == spawnpoint.GetComponent<SpawnPoint>().GetIndex() + 1)
        {
            spawnpoint = _NewSpawnPoint;
            spawnpointsCleared++;
            _NewSpawnPoint.transform.Find("flag").gameObject.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        }
    }
    public bool AllCheckpointsCleared()
    {
        if(spawnpointsCleared == AllSpawnPoints.transform.childCount)
        {
            return true;
        }
        return false;
    }

    public void ResetSpawnpointClearedCount()
    {
        spawnpointsCleared = 0;
    }

    public void Respawn()
    {
        if (spawnpoint != null)
        {
            transform.Find("Player").gameObject.transform.position = new Vector3(spawnpoint.transform.position.x, spawnpoint.transform.position.y, 0) + new Vector3(1, 0, 0);
            transform.Find("Player2").gameObject.transform.position = new Vector3(spawnpoint.transform.position.x, spawnpoint.transform.position.y, 0) + new Vector3(-1, 0, 0);
            cam.gameObject.transform.position = spawnpoint.transform.position + new Vector3(1, 0, -35);
        }    
    }
}
