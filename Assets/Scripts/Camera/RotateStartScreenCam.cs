using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateStartScreenCam : MonoBehaviour
{
    [SerializeField] float RotateSpeed;
    private void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, RotateSpeed, 0));
    }
}
