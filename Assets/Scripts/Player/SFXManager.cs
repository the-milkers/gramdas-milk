using UnityEngine;
using UnityEngine.Audio;

namespace Player
{
    public class SFXManager : MonoBehaviour
    {
        [SerializeField] private AudioSource gameAudio;
        [SerializeField] private AudioSource outOfTimeAudio;
        [SerializeField] private AudioSource gameOverAudio;
        [SerializeField] private AudioSource pausedAudio;
        [SerializeField] private AudioMixerSnapshot outOfTimeSnapshot;
        [SerializeField] private AudioMixerSnapshot musicSnapshot;
        [SerializeField] private AudioMixerSnapshot gameOverSnapshot;
        [SerializeField] private AudioMixerSnapshot pausedSnapshot;

        private const float TransitionIn = .25f;
        private const float TransitionOut = 0f;

        private void Start()
        {
            musicSnapshot.TransitionTo(TransitionOut);
        }

        public void PlayGameOverAudio()
        {
            gameOverSnapshot.TransitionTo(TransitionIn);
            gameOverAudio.Play();
        }

        public void PlayOutOfTimeAudio()
        {
            outOfTimeSnapshot.TransitionTo(TransitionIn);
            outOfTimeAudio.Play();
        }

        public void PlayPausedAudio()
        {
            pausedSnapshot.TransitionTo(0f);
            pausedAudio.Play();
            gameAudio.Pause();
        }

        public void StopPausedAudio()
        {
            musicSnapshot.TransitionTo(TransitionOut + .5f);
            gameAudio.UnPause();
        }
    }
}
