using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearSettings : MonoBehaviour
{

    [Header("PrimaryGear")]
    public KeyCode Player1ActivationPM;
    public KeyCode Player2ActivationPM;

    [Header("SecondaryGear")]
    public KeyCode Player1ActivationSD;
    public KeyCode Player2ActivationSD;



    public void onDeath()
    {
        if (FindObjectsOfType<Gear>().Length > 0)
        {
            foreach (var boost in FindObjectsOfType<Boost>())
            {
                if (boost.isActiveAndEnabled) boost.onDeath();
            }
        }  
        if(FindObjectOfType<Dash>() != null) FindObjectOfType<Dash>().onDeath();
        FindObjectOfType<GearUI>().ResetGear();

    }


    public KeyCode getP1PrimaryKeyCode()
    {
        return Player1ActivationPM;
    }
    public KeyCode getP2PrimaryKeyCode()
    {
        return Player2ActivationPM;
    }
    public KeyCode getP1SecondaryKeyCode()
    {
        return Player1ActivationSD;
    }
    public KeyCode getP2SecondaryKeyCode()
    {
        return Player2ActivationSD;
    }
}
