using Player;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Util;

public class LevelEnd : MonoBehaviour
{
    [SerializeField] Canvas LevelClearedCanvas;

    RespawnHandler respawnhandler;
    SceneLoader SceneLoader;
    PointSystem pointsystem;
    bool player1Finished;
    bool player2Finished;
    bool levelcleared;
    int _currentcoins;
    string _timecompleted;
    float _currentpoints;
    int _deaths;

    float coinpoints;
    float deathpoints;
    float timepoints;
    float totalpoints;
    private PauseGame _pauseGame;

    private PlayerHealth[] Players;



    private void Start()
    {
        LevelClearedCanvas.gameObject.SetActive(false);
        respawnhandler = FindObjectOfType<RespawnHandler>();
        SceneLoader = FindObjectOfType<SceneLoader>();
        pointsystem = FindObjectOfType<PointSystem>();
        _pauseGame = FindObjectOfType<PauseGame>();
    }

    private void Update()
    {
        if (levelcleared && (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button0))) 
        {
            SceneLoader.LoadMenu();
        }
    }

    private void FinishLevel()
    {
        _pauseGame.SetHasMenuOpen(true);
        //getting stats for levelcleared panel
        _currentcoins = FindObjectOfType<CoinManager>().GetCurrentCoins();
        _timecompleted = FindObjectOfType<Timer>().GetCurrentTime();
        timepoints = pointsystem.GetTotalPoints();

        //_deaths = FindObjectOfType<DeathHandler>().GetCurrentDeaths();
        Players = FindObjectsOfType<PlayerHealth>();
        foreach(var player in Players)
        {
            _deaths += player.GetPlayerDeaths();
        }

        //adding and subtracting points
        coinpoints = _currentcoins * 100;
        deathpoints = _deaths * -1000;
        

        pointsystem.Addpoints(coinpoints);
        pointsystem.Addpoints(deathpoints);

        //updating stats panel
        UpdateLevelClearedText(_currentcoins, _deaths, pointsystem.GetTotalPoints(), _timecompleted);

        //level cleared handling
        Time.timeScale = 0;
        LevelClearedCanvas.gameObject.SetActive(true);
        player1Finished = false;
        player2Finished = true;
        respawnhandler.ResetSpawnpointClearedCount();
        levelcleared = true;
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.name == "Player")
        {
            player1Finished = true;
        }
        if (other.gameObject.name == "Player2")
        {
            player2Finished = true;
        }
        if(player1Finished && player2Finished && respawnhandler.AllCheckpointsCleared())
        {
            FinishLevel();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            player1Finished = false;
        }
        if (other.gameObject.name == "Player2")
        {
            player2Finished = false;
        }
    }



    public void UpdateLevelClearedText(int coins, int deaths, float points, string time)
    {
        //Ugly ass code
        GameObject statscanvas = LevelClearedCanvas.transform.Find("CurrentStats").gameObject;

        GameObject CurrentCoins = statscanvas.transform.Find("CoinsCollectedTxt").gameObject;
        GameObject CurrentDeaths = statscanvas.transform.Find("DeathsTxt").gameObject;
        GameObject CurrentPoints = statscanvas.transform.Find("CurrentTotalPoints").transform.Find("TotalPointsTxt").gameObject;
        GameObject CurrentTime = statscanvas.transform.Find("TimeTxt").gameObject;

        GameObject deathpointsTxt = CurrentDeaths.transform.Find("Deathpoints").gameObject;
        GameObject coinpointsTxt = CurrentCoins.transform.Find("Coinpoints").gameObject;
        GameObject timePointsTxt = CurrentTime.transform.Find("TimePoints").gameObject;

        CurrentCoins.GetComponent<TextMeshProUGUI>().text = "Coins Collected: " + coins;
        CurrentDeaths.GetComponent<TextMeshProUGUI>().text = "Deaths: " + deaths;
        CurrentPoints.GetComponent<TextMeshProUGUI>().text = Mathf.Round(points).ToString();
        CurrentTime.GetComponent<TextMeshProUGUI>().text = "Time: " + time;

        deathpointsTxt.GetComponent<TextMeshProUGUI>().text = deathpoints.ToString();
        coinpointsTxt.GetComponent<TextMeshProUGUI>().text = "+ " + coinpoints;
        timePointsTxt.GetComponent<TextMeshProUGUI>().text = "+ " + Mathf.Round(timepoints);
    }
}
