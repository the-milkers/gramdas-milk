using Player;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointSystem : MonoBehaviour
{
    [SerializeField] float TotalPoints;
    [SerializeField] TextMeshProUGUI pointstext;
    float _startingPoints;
    float _LerpValue;
    float _timeElapsed;
    float _totalTime;
    bool _timerFound;
    bool _playing = true;


    private void Start()
    {
        _startingPoints = TotalPoints;
    }

    private void Update()
    {
        if(FindObjectOfType<PlayerInputHandler>() != null && _playing)
        {
            if (!_timerFound)
            {
                _totalTime = FindObjectOfType<Timer>().GetTotalTime();
                if(_totalTime != 0)
                {
                    _timerFound = true;
                }
            }
            if (TotalPoints > 0 && _timerFound)
            {
                PointsForTime();
                UpdatePointsText();
            }
        }
        
    }

    public void PointsForTime()
    {
        if(_timeElapsed <= _totalTime)
        {
            _LerpValue = Mathf.Lerp(_startingPoints, 0, _timeElapsed / _totalTime);
            _timeElapsed += Time.deltaTime;
            TotalPoints = _LerpValue;
        }
        else
        {
            TotalPoints = 0;
        }
        
    }

    public float GetTotalPoints()
    {
        _playing = false;
        if(TotalPoints < 0)
        {
            TotalPoints = 0;
        }
        return TotalPoints;
    }

    public void onFullDeath()
    {
        _playing = false;
    }

    public void UpdatePointsText()
    {
        pointstext.text = Mathf.Round(TotalPoints) + " Pts"; 
    }

    public void Addpoints(float points)
    {
        TotalPoints += points;
    }

}
