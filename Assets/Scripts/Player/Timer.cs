using Player;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Util;

public class Timer : MonoBehaviour
{
    [SerializeField] float TimeRemaining;
    [SerializeField] TextMeshProUGUI timertext;
    [SerializeField] private Camera mainCamera;
    [SerializeField] Canvas timeoverCanvas;
    [SerializeField] GameObject players;
    [SerializeField] TextMeshProUGUI TimeOverText;
    bool _startTimer = true;
    float _totalTime;
    float timeElapsed;
    float lerpduration = 10;
    float timeTillRestart = 5;
    
    private PauseGame _pauseGame;
    private SFXManager _sfx;
    private bool _hasPlayed;
    
    private void Start()
    {
        _totalTime = TimeRemaining;
        _pauseGame = FindObjectOfType<PauseGame>();
        timeoverCanvas.gameObject.SetActive(false);
        _sfx = FindObjectOfType<SFXManager>();
        _hasPlayed = false;
    }

    private void Update()
    {
        if (FindObjectOfType<PlayerInputHandler>() != null && _startTimer)
        {
            if (TimeRemaining >= .1f)
            {
                TimeRemaining -= Time.deltaTime;
            }
            else
            {
                OnTimeRunOut();
            }
        }
        timertext.text = DisplayTime(TimeRemaining);
        if (TimeRemaining <= 30)
        {
            ChangeFontAndColor();
        }
    }

    public void StartTimer()
    {
        _startTimer = true;
    }

    public void ChangeFontAndColor()
    {
        timertext.color = Color.red;
        if (timeElapsed < lerpduration)
        {
            float lerpvalue = Mathf.Lerp(70, 100, timeElapsed / lerpduration);
            timeElapsed += Time.deltaTime;
            timertext.fontSize = lerpvalue;
        }
    }

    private void OnTimeRunOut()
    {
        foreach (var playermovement in players.GetComponentsInChildren<PlayerMovement>())
        {
            playermovement.enabled = false;
        }

        if (!_hasPlayed)
        {
            _hasPlayed = true;
            _sfx.PlayOutOfTimeAudio();
        }
        mainCamera.GetComponent<Blur>().enabled = false;
        timeoverCanvas.gameObject.SetActive(true);
        _pauseGame.SetHasMenuOpen(true);
        TimeRemaining = 0;
        timeTillRestart -= Time.deltaTime;
        TimeOverText.text = "Restarting in " + Mathf.RoundToInt(timeTillRestart) + " Seconds...";
        if (timeTillRestart <= .1f)
        {
            FindObjectOfType<SceneLoader>().LoadScene();
        }
    }

    public float EndTimer()
    {
        _startTimer = false;
        return TimeRemaining;
    }

    public float GetTotalTime()
    {
        return _totalTime;
    }

    public string GetCurrentTime()
    {
        return DisplayTime(_totalTime - TimeRemaining);
    }

    public string DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliSeconds = (timeToDisplay % 1) * 1000;
        
        // ugly ass code
        if (minutes <= 10)
        {
            if (minutes <= 0)
            {
                if (seconds <= 10)
                {
                    return string.Format("{0:0}.{1:000}", seconds, milliSeconds);
                }
                return string.Format("{0:00}.{1:000}", seconds, milliSeconds);
            }
            return string.Format("{0:0}:{1:00}.{2:000}", minutes, seconds, milliSeconds);
        }
        return string.Format("{00:0}:{1:00}.{2:000}", minutes, seconds, milliSeconds);
    }

}
