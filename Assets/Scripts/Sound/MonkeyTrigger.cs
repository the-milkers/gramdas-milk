using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MonkeyTrigger : MonoBehaviour
{
    [Header("Audio")]
    public AudioMixerSnapshot MonkeySnapshot;
    public AudioMixerSnapshot NatureSnapshot;
    //public AudioSource MonkeySource;

    [Header("Settings")]
    public float TransistionIn;
    public float TransitionOut;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            MonkeySnapshot.TransitionTo(TransitionOut);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            NatureSnapshot.TransitionTo(TransitionOut);
        }
    }
}
