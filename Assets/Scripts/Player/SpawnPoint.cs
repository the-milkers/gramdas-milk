using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    [SerializeField] int index;
    
    private bool player1Activated;
    private bool player2Activated;

    private void Start()
    {
        if(index == 0)
        {
            transform.Find("flag").gameObject.GetComponent<Renderer>().material.color = new Color(255, 0, 0);
        }
    }
    public int GetIndex()
    {
        return index;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            player1Activated = true;
        }
        if (other.gameObject.name == "Player2")
        {
            player2Activated = true;
        }
        if (player1Activated && player2Activated && index != 0)
        {
            SetSpawnpoint(other);
        }   
        
    }

    public void OnDeath()
    {
        if (!player1Activated || !player2Activated)
        {
            player1Activated = false;
            player2Activated = false;
        }       
}


    private void SetSpawnpoint(Collider other)
    {
        if(other != null)
        {
            FindObjectOfType<RespawnHandler>().ChangeSpawnPoint(gameObject);
        } 
    }
}
