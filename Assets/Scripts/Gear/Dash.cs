using Player;
using UnityEngine;

public class Dash : MonoBehaviour
{
    public enum Player { player1, player2 }

    private const string PrimaryGearKey = "_PrimaryGear";
    private const string SecondaryGearKey = "_SecondaryGear";

    [Header("Cooldown")]
    public float activationcooldown;
    public float activationCDTimer;

    [Header("Timer")]
    public float activationDuration;
    public float activationTimer;

    PlayerMovement _player1;
    PlayerMovement _player2;

    private KeyCode _Player1Activation;
    private KeyCode _Player2Activation;

    private int _index;
    private Gear _gear;
    private GearSettings _gearSettings;

    Player _currentplayer;
    bool _dashing;

    bool _primary;
    private void Start()
    {
        _gear = GetComponent<Gear>();
        _gearSettings = FindObjectOfType<GearSettings>();

        foreach (var player in FindObjectsOfType<PlayerMovement>())
        {
            if (player.GetPlayerName() == "Player") _player1 = player;
            if (player.GetPlayerName() == "Player2") _player2 = player;
        }
        _index = _gear.GetIndex();
        SetKeyCodes();     
    }

    private void Update()
    {
        //Debug.Log(_player1.IsColliding());
        if (FindObjectOfType<PlayerInputHandler>() == null) return;

        if (Input.GetKeyDown(_Player1Activation))
        {
            ActivateDash();
            _currentplayer = Player.player1;
        }
        if (Input.GetKeyDown(_Player2Activation))
        {
            ActivateDash();
            _currentplayer = Player.player2;         
        }

        if (activationCDTimer > 0) activationCDTimer -= Time.deltaTime;

        if (_dashing && activationTimer > 0) activationTimer -= Time.deltaTime;

        if (activationTimer < 0) EndDash();

        if (_dashing) StartDash(_currentplayer);
    }

    private void ActivateDash()
    {
        if (activationCDTimer > 0) return;
        FindObjectOfType<GearUI>().OnActivation(_primary);
        _dashing = true;
    }

    private void StartDash(Player player)
    {
        if(player == Player.player1 && !_player1.IsColliding())
        {
            if(_player1.GetVelocity().x >= 0) _player1.transform.position += new Vector3(.3f, 0);
            if (_player1.GetVelocity().x < 0) _player1.transform.position += new Vector3(-.3f, 0);
            _player1.ResetVelocity();
        }
        if (player == Player.player2 && !_player2.IsColliding())
        {
            if (_player2.GetVelocity().x >= 0) _player2.transform.position += new Vector3(.3f, 0);
            if (_player2.GetVelocity().x < 0) _player2.transform.position += new Vector3(-.3f, 0);
            _player2.ResetVelocity();
        }
    }

    private void EndDash()
    {
        FindObjectOfType<GearUI>().OnCoolDownStart(_primary, activationcooldown);
        _dashing = false;
        activationCDTimer = activationcooldown;
        activationTimer = activationDuration;
    }

    public void onDeath()
    {
        _dashing = false;
        activationCDTimer = 0;
        activationTimer = activationDuration;
    }

    private void SetKeyCodes()
    {
        if (_index == PlayerPrefs.GetInt(PrimaryGearKey))
        {
            _Player1Activation = _gearSettings.getP1PrimaryKeyCode();
            _Player2Activation = _gearSettings.getP2PrimaryKeyCode();
            _primary = true;
        }
        else if (_index == PlayerPrefs.GetInt(SecondaryGearKey))
        {
            _Player1Activation = _gearSettings.getP1SecondaryKeyCode();
            _Player2Activation = _gearSettings.getP2SecondaryKeyCode();
            _primary = false;
        }
    }
}
