﻿//using System.Collections;
//using System.Collections.Generic;

using System;
using UnityEngine;

public class dg_simpleCamFollow : MonoBehaviour
{
    [SerializeField] private bool isZoomedOut;
    [SerializeField] private float normalFov = 20f;
    [SerializeField] private float zoomedOutFov = 30f;
    [SerializeField] private float zoomSpeed;
    
    public Transform target;
    [Range(1f,40f)] public float laziness = 10f;
    public bool lookAtTarget = true;
    public bool takeOffsetFromInitialPos = true;
    public Vector3 generalOffset;
    private Camera _camera;
    private Vector3 _whereCameraShouldBe;
    private bool _warningAlreadyShown;
    private int _currentLockedAxis;
    private float _xLock = 0f;
    private float _yLock = 0f;

    private void Start() {
        if (takeOffsetFromInitialPos && target != null) generalOffset = transform.position - target.position;
        _camera = Camera.main;
    }

    //TODO keep the right x or y axis
    private void FixedUpdate()
    {
        if (target != null) {
            switch (_currentLockedAxis)
            {
                case 0:
                    _whereCameraShouldBe = target.position + generalOffset;
                    break;
                case 1:
                    _whereCameraShouldBe = new Vector3(_xLock, target.position.y) + generalOffset;
                    break;
                case 2:
                    _whereCameraShouldBe = new Vector3(target.position.x, _yLock) + generalOffset;
                    break;
                case 3:
                    _whereCameraShouldBe = new Vector3(_xLock, _yLock) + generalOffset;
                    break;
            }
            transform.position = Vector3.Lerp(transform.position, _whereCameraShouldBe, 1 / laziness);

            if (lookAtTarget) transform.LookAt(target);
        } else {
            if (_warningAlreadyShown) return;
            Debug.Log("Warning: You should specify a target in the simpleCamFollow script.", gameObject);
            _warningAlreadyShown = true;
        }
    }

    private void LateUpdate()
    {
        _camera.fieldOfView = Mathf.Lerp(_camera.fieldOfView, isZoomedOut ? zoomedOutFov : normalFov, zoomSpeed);
    }

    public void ChangeTarget(Transform newTarget)
    {
        target = newTarget;
    }

    public Transform GetCurrentTarget()
    {
        return target;
    }

    public void SetZoomedOut(bool b)
    {
        isZoomedOut = b;
    }

    public void SetCurrentLockedAxis(int i)
    {
        _currentLockedAxis = i;
    }

    public void SetCurrentLockedAxis(int i, float xLock, float yLock)
    {
        _currentLockedAxis = i;
        _xLock = xLock;
        _yLock = yLock;
    }
}
