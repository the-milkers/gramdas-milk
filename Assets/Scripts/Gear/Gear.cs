using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class Gear : MonoBehaviour
{
    [SerializeField] int Index;

    private const string PrimaryGearKey = "_PrimaryGear";
    private const string SecondaryGearKey = "_SecondaryGear";


    private void Start()
    {
        gameObject.SetActive(false);
        if (Index == PlayerPrefs.GetInt(PrimaryGearKey)) gameObject.SetActive(true);
        if (Index == PlayerPrefs.GetInt(SecondaryGearKey)) gameObject.SetActive(true);
    }

    public int GetIndex()
    {
        return Index;
    }
}
