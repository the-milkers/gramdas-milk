using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private LayerMask groundLayers;
        [SerializeField] private float speed = 2.25f;
        [SerializeField] private float jumpForce = 17.5f;
        [SerializeField] private int playerIndex = 0;
        
        private Rigidbody _rigidbody;
        private Vector3 _velocity;
        private bool _isGrounded;
        private Vector2 _inputVector = Vector2.zero;
        private BoxCollider _collider;
        private const float CoyoteTime = 0.1f;
        private float _coyoteTimeCounter;

        private bool _isColliding;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _collider = GetComponent<BoxCollider>();
        }

        public int GetPlayerIndex()
        {
            return playerIndex;
        }

        public void ResetVelocity()
        {
            _rigidbody.velocity = Vector3.zero;
            _velocity = Vector3.zero;
        }
        
        public void SetInputVector(Vector2 direction)
        {
            _inputVector = direction;
        }

        private void FixedUpdate()
        {
            _isGrounded = Physics.CheckBox(transform.position, new Vector3((float) (_collider.size.x/2.5),0.01f), Quaternion.identity, groundLayers, QueryTriggerInteraction.Ignore);
            _velocity = new Vector3(_inputVector.x * speed, 0, 0);

            if (_isGrounded)
            {
                _rigidbody.velocity = Vector3.zero;
                _coyoteTimeCounter = CoyoteTime;
            }
            else
            {
                _coyoteTimeCounter -= Time.deltaTime;
            }
            
            if (_coyoteTimeCounter > 0f && _inputVector.y > 0)
            {
                _rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
                _coyoteTimeCounter = 0f;
            }

            // actually move
            _rigidbody.MovePosition(transform.position + _velocity * (Time.deltaTime * speed));
            
            // change rotation
            if (_velocity.x < 0)
            {
                transform.GetChild(2).localScale = new Vector3(9, 9, -9);
            } else if (_velocity.x > 0)
            {
                transform.GetChild(2).localScale = new Vector3(9, 9, 9);
            }
        }

        public void SetJumpForce(float newForce)
        {
            jumpForce = newForce;
        }

        public void SetSpeed(float newSpeed)
        {
            speed = newSpeed;
        }

        public string GetPlayerName()
        {
            return gameObject.name;
        }

        public float GetSpeed()
        {
            return speed;
        }

        public float GetJumpForce()
        {
            return jumpForce;
        }

        public Vector3 GetVelocity()
        {
            return _velocity;
        }

        public bool IsColliding()
        {
            return _isColliding;
        }

        private void OnCollisionEnter(Collision collision)
        {
            _isColliding = true;
        }

        private void OnCollisionExit(Collision collision)
        {
            _isColliding = false;
        }
    }
}
