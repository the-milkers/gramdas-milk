using Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathHandler : MonoBehaviour
{

    int _deaths;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && other.GetComponent<PlayerMovement>() != null)
        {
            HandleDeath(other);
        }   
    }

    private void HandleDeath(Collider other)
    {
        other.gameObject.GetComponent<PlayerHealth>().OnTakeDamage();
        FindObjectOfType<RespawnHandler>().Respawn();
        FindObjectOfType<GearSettings>().onDeath();
        foreach (var spawnpoint in FindObjectsOfType<SpawnPoint>())
        {
            spawnpoint.OnDeath();
        }
        foreach (var box in FindObjectsOfType<Box>())
        {
            box.ResetPosition();
        }
        other.gameObject.GetComponent<PlayerMovement>().ResetVelocity();
        _deaths++;
    }

    public int GetCurrentDeaths()
    {
        return _deaths;
    }
}
