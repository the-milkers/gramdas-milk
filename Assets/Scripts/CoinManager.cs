using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    [SerializeField] private GameObject pickups;
    [SerializeField] private TextMeshProUGUI coinText;

    private int _currentCoins;
    private int _maxCoins;

    private void Start()
    {
        _maxCoins = pickups.transform.childCount;
        coinText.text = _currentCoins + " / " + _maxCoins + " Coins";
    }

    public void OnCoinPickup(int coinValue)
    {
        _currentCoins += coinValue;
        coinText.text = _currentCoins + " / " + _maxCoins + " Coins";
    }

    public int GetCurrentCoins()
    {
        return _currentCoins;
    }
}
