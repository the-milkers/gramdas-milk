using System;
using UnityEngine;

public class Box : MonoBehaviour
{
    private Vector3 _position;
    private void Awake()
    {
        _position = transform.position;
    }

    public void ResetPosition()
    {
        transform.position = _position;
    }
}
