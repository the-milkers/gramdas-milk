using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectGear : MonoBehaviour
{
    [SerializeField] Canvas LevelSelectCanvas;

    private const string PrimaryGearKey = "_PrimaryGear";
    private const string SecondaryGearKey = "_SecondaryGear";

    GameObject _levelselectpanel;
    GameObject _GearselectPanel;

    GameObject _Gear1btn;
    GameObject _Gear2btn;
    GameObject _Gear3btn;

    List<int> _GearSelected;

    int _primaryGear;
    int _secondaryGear;


    void Start()
    {
        _levelselectpanel = LevelSelectCanvas.transform.Find("LevelSelectPanel").gameObject;
        _GearselectPanel = _levelselectpanel.transform.Find("GearPanel").gameObject;

        _Gear1btn = _GearselectPanel.transform.Find("Gear1BTN").gameObject;
        _Gear2btn = _GearselectPanel.transform.Find("Gear2BTN").gameObject;
        _Gear3btn = _GearselectPanel.transform.Find("Gear3BTN").gameObject;

        _GearSelected = new List<int>();

        setColor();
    }

    public void OnLevelStart()
    {
        switch (_GearSelected.Count)
        {
            case 2:
                _primaryGear = _GearSelected[0];
                _secondaryGear = _GearSelected[1];
                break;
            case 1:
                _primaryGear = _GearSelected[0];
                _secondaryGear = 0;
                break;
            case 0:
                _primaryGear = 0;
                _secondaryGear = 0;
                break;
            default:
                return;
        }


        PlayerPrefs.SetInt(PrimaryGearKey, _primaryGear);
        PlayerPrefs.SetInt(SecondaryGearKey, _secondaryGear);
    }


    public void Selectgear(int index)
    {
        switch (index)
        {
            case 1:
                if (_Gear1btn.GetComponent<Toggle>().isOn && _GearSelected.Count != 2) _GearSelected.Add(index);
                else
                {
                    _Gear1btn.GetComponent<Toggle>().isOn = false;
                    _GearSelected.Remove(index);
                }
                break;
            case 2:
                if (_Gear2btn.GetComponent<Toggle>().isOn && _GearSelected.Count != 2) _GearSelected.Add(index);
                else
                {
                    _Gear2btn.GetComponent<Toggle>().isOn = false;
                    _GearSelected.Remove(index);
                }
                break;
            case 3:
                if (_Gear3btn.GetComponent<Toggle>().isOn && _GearSelected.Count != 2) _GearSelected.Add(index);
                else
                {
                    _Gear3btn.GetComponent<Toggle>().isOn = false;
                    _GearSelected.Remove(index);
                }
                break;
        }
        setColor();
    }

    private void setColor()
    {
        if (_Gear1btn.GetComponent<Toggle>().isOn) _Gear1btn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _Gear1btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);

        if (_Gear2btn.GetComponent<Toggle>().isOn) _Gear2btn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _Gear2btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);

        if (_Gear3btn.GetComponent<Toggle>().isOn) _Gear3btn.GetComponent<Image>().color = new Color(0.24f, 0.81f, 0.27f, 0.3f);
        else _Gear3btn.GetComponent<Image>().color = new Color(1, 1, 1, 0.3f);
    }
}
