using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] GameObject players;

    Transform _player1;
    Transform _player2;
    dg_simpleCamFollow _camFollow;

    private void Start()
    {
        _player1 = players.transform.Find("Player").transform;
        _player2 = players.transform.Find("Player2").transform;
        _camFollow = GetComponent<dg_simpleCamFollow>();
    }
    public void Update()
    {
        if (_player2.position.x > _player1.position.x && _camFollow.GetCurrentTarget() == _player1)
        {
            _camFollow.ChangeTarget(_player2);
        }
        else if(_player1.position.x > _player2.position.x && _camFollow.GetCurrentTarget() == _player2)
        {
            _camFollow.ChangeTarget(_player1);
        }  
    }
}
