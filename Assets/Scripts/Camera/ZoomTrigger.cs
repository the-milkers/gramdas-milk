using System;
using System.Collections.Generic;
using UnityEngine;

public class ZoomTrigger : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private LockedAxis lockedAxis;
    [SerializeField] private float xAxisLock;
    [SerializeField] private float yAxisLock;
    
    private enum LockedAxis
    {
        None, X, Y, Both
    }

    private dg_simpleCamFollow _camFollow;

    private void Awake()
    {
        _camFollow = mainCamera.GetComponent<dg_simpleCamFollow>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.transform.CompareTag("Player")) return;
        _camFollow.SetCurrentLockedAxis(lockedAxis.GetHashCode(), xAxisLock, yAxisLock);
        _camFollow.SetZoomedOut(true);
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.transform.CompareTag("Player")) return;
        _camFollow.SetCurrentLockedAxis(0);
        _camFollow.SetZoomedOut(false);
    }
}
