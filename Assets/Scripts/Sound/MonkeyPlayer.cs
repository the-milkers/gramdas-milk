using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonkeyPlayer : MonoBehaviour
{
    public AudioClip[] monkeys;
    public AudioSource monkeySource;

    private int selectmonkey;
    private float timer;
    private float time;

    // Start is called before the first frame update
    void Start()
    {
        selectmonkey = Random.Range(0, monkeys.Length);
        time = Random.Range(2.5f, 5f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer > time)
        {
            monkeySource.clip = monkeys[selectmonkey];
            monkeySource.Play();

            timer = 0;
            selectmonkey = Random.Range(0, monkeys.Length);
            time = Random.Range(1.5f, 5f) + monkeySource.clip.length;

        }
    }
}
