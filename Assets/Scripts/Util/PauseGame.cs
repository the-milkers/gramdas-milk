using System;
using Player;
using Unity.VisualScripting;
using UnityEngine;

namespace Util
{
    public class PauseGame : MonoBehaviour
    {
        [SerializeField] private Canvas pauseCanvas;
        public bool isPaused;

        private bool _hasMenuOpen;
        private SceneLoader _sceneLoader;
        private SFXManager _sfx;

        private void Awake()
        {
            _sceneLoader = GetComponent<SceneLoader>();
            pauseCanvas.gameObject.SetActive(false);
            _sfx = FindObjectOfType<SFXManager>();
        }

        private void Update()
        {
            switch (isPaused)
            {
                case false when !_hasMenuOpen && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7)):
                    isPaused = true;
                    pauseCanvas.gameObject.SetActive(true);
                    Time.timeScale = 0;
                    _sfx.PlayPausedAudio();
                    break;
                case true when Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) ||
                               Input.GetKeyDown(KeyCode.JoystickButton0) || Input.GetKeyDown(KeyCode.Joystick1Button7):
                    isPaused = false;
                    pauseCanvas.gameObject.SetActive(false);
                    Time.timeScale = 1;
                    _sfx.StopPausedAudio();
                    break;
                case true when Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.Joystick1Button1):
                    SceneLoader.LoadMenu();
                    break;
                case true when Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Joystick1Button3):
                    _sceneLoader.LoadScene();
                    break;
            }
        }

        public bool GetHasMenuOpen()
        {
            return _hasMenuOpen;
        }

        public void SetHasMenuOpen(bool isOpened)
        {
            _hasMenuOpen = isOpened;
        }
    }
}
